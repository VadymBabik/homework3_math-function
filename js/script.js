function checkNumber(val) {
  if (typeof val === "number" && !Number.isNaN(val)) {
    return true;
  }
  return false;
}

let firstNumber = +prompt("Enter you first number:");
let secondNumber = +prompt("Enter you second number:");

while (!checkNumber(firstNumber)) {
  firstNumber = +prompt(
    "You entered an incorrect value! Enter a first number:"
  );
}

while (!checkNumber(secondNumber)) {
  secondNumber = +prompt(
    "You entered an incorrect value! Enter a second number:"
  );
}
let userOperation;
do userOperation = prompt("Enter Operation +, -, *, /:");
while (
  userOperation !== "+" &&
  userOperation !== "-" &&
  userOperation !== "*" &&
  userOperation !== "/"
);

function calculate(a, b, c) {
  switch (c) {
    case "+":
      return a + b;
    case "-":
      return a - b;
    case "*":
      return a * b;
    case "/":
      return a / b;
    default:
      return "Error";
  }
}

console.log(calculate(firstNumber, secondNumber, userOperation));
